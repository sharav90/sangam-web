import { Component, OnInit, Optional, Inject } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource
} from "@angular/material";
import { Sangam } from "../model/sangam";
import { Organizer } from "../model/organizer";
import { OrganizerService } from "../service/organizer.service";
import { USER_NAME, SANGAM_ID, ORGANIZER_ID } from "../util/app.constants";
import { strictEqual } from "assert";

export interface SangamInfo {
  id: number;
  name: string;
  createdTs: Date;
  isActive: boolean;
}

@Component({
  selector: "app-sangam-dialog",
  templateUrl: "./sangam-dialog.component.html",
  styleUrls: ["./sangam-dialog.component.css"]
})
export class SangamDialogComponent implements OnInit {
  public organizer: Organizer;
  // displayedColumns: string[] = ["id", "name", "createdTs", "isActive"];
  // sangamInfo: SangamInfo[];
  // dataSource = new MatTableDataSource(this.sangamInfo);

  constructor(
    private dialogRef: MatDialogRef<SangamDialogComponent>,
    private organizerService: OrganizerService
  ) {
    this.getOrganizerSangamList();
    dialogRef.disableClose = true;
  }

  ngOnInit() {}

  getOrganizerSangamList() {
    this.organizerService
      .getSangamListByOrganizerName(sessionStorage.getItem(USER_NAME))
      .subscribe(
        data => {
          this.organizer = data;
          // this.loadTableData();
        },
        error => {
          console.log(error);
        }
      );
  }

  // loadTableData() {
  //   this.organizer.sangamDto.forEach(s => {
  //     let smInfo = <SangamInfo>{
  //       id: s.id,
  //       name: s.name,
  //       createdTs: s.createdTs,
  //       isActive: s.isActive
  //     };
  //     console.log(smInfo);
  //     this.sangamInfo.push(smInfo);
  //   });
  //   console.log(this.sangamInfo);
  // }

  onSelectSangam(sangamId: string): void {
    sessionStorage.setItem(SANGAM_ID, sangamId);
    console.log("Sangam Id=" + sangamId);
    sessionStorage.setItem(ORGANIZER_ID, this.organizer.id.toString());
    console.log("Organizer Id=" + this.organizer.id);
    this.dialogRef.close();
  }
}
