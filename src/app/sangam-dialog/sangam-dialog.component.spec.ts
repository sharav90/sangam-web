import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SangamDialogComponent } from './sangam-dialog.component';

describe('SangamDialogComponent', () => {
  let component: SangamDialogComponent;
  let fixture: ComponentFixture<SangamDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SangamDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SangamDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
