import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ErrorComponent } from './error/error.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { RouteguardService } from './service/routeguard.service';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';
import { ReportComponent } from './report/report.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login',    component: LoginComponent },
  { path: 'dashboard',component: DashboardComponent, canActivate: [RouteguardService] },
  { path: 'editprofile', component: EditprofileComponent, canActivate: [RouteguardService] },
  { path: 'registration',component: RegisterComponent, canActivate: [RouteguardService] },
  { path: 'settings', component: SettingsComponent, canActivate: [RouteguardService] },
  { path: 'reports' , component: ReportComponent, canActivate: [RouteguardService] },
  { path: 'payments', component: PaymentComponent, canActivate: [RouteguardService] },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
