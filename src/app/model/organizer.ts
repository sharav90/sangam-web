import { Sangam } from "./sangam";

export class Organizer {
  constructor(
    public id: number,
    public name: string,
    public mobile: string,
    public passcode: string,
    public aadharNo: string,
    public dateOfBirth: string,
    public address: string,
    public isActive: boolean,
    public createdTs: Date,
    public lastUpdateTs: Date,
    public sangamDto: Sangam[]
  ) {}

  get getId() : number {
    return this.id;
  }
}
