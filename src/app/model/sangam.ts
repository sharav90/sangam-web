export class Sangam {
  constructor(
    public id: number,
    public name: string,
    public bankAccNo: string,
    public bankName: string,
    public ifscCode: string,
    public isActive: boolean,
    public createdTs: Date,
    public lastUpdateTs: Date
  ) {}
}
