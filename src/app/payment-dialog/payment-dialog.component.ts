import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PaymentDto } from '../payment/payment.component';

@Component({
  selector: 'app-payment-dialog',
  templateUrl: './payment-dialog.component.html',
  styleUrls: ['./payment-dialog.component.css']
})
export class PaymentDialogComponent implements OnInit {

  dateNow: Date = new Date();
  paymentDto: PaymentDto;
  message: string = "";

  constructor(
    private dialogRef: MatDialogRef<PaymentDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: PaymentDto) {
    console.log("dialog => ", data);
    this.paymentDto = data;
    dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  onSave() {
    this.message = 'Data saved succcessfully.';
  }

}

