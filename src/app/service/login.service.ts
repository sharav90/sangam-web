import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LOGGED_IN_USER, JWT_TOKEN, AUTHENTICATE_API, USER_NAME } from '../util/app.constants';
import { Authentication } from '../model/authentication';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private http: HttpClient) { }

  jwtAuthenticate(jwtAuthRequest: Authentication) {
    console.log('login service class')
    let username = jwtAuthRequest.username;
    let password = jwtAuthRequest.password;
    return this.http.post<any>(AUTHENTICATE_API, { username, password }).
    pipe(map (
      data => {
        sessionStorage.setItem(LOGGED_IN_USER, jwtAuthRequest.username);
        let token = `Bearer ${data.token}`;
        sessionStorage.setItem(JWT_TOKEN, token);
        sessionStorage.setItem(USER_NAME, username);
        return data;
      }
    ));
  }
 
  logout(): void {
    sessionStorage.removeItem(LOGGED_IN_USER);
    sessionStorage.removeItem(JWT_TOKEN);
  }

  getLoggedInUsername(): string {
    return sessionStorage.getItem(LOGGED_IN_USER);
  }

  getJwtToken(): string {
    return sessionStorage.getItem(JWT_TOKEN);
  }

  isUserLoggedIn(): boolean {
    return this.getLoggedInUsername() != null;
  }

}
