import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { GET_ORGANIZER_BY_ID_API } from "../util/app.constants";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class OrganizerService {
  constructor(private http: HttpClient) {}

  getSangamListByOrganizerName(organizerName: string): Observable<any> {
    return this.http.get<any>(GET_ORGANIZER_BY_ID_API, {
      params: { name: organizerName }
    });
  }
}
