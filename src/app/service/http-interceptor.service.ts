import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private loginSvc: LoginService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let jwtToken = this.loginSvc.getJwtToken();
    if(jwtToken && this.loginSvc.isUserLoggedIn()) {
      request = request.clone({
        setHeaders : {
          Authorization : jwtToken
        }
      });
    }
    return next.handle(request);
  }
}
