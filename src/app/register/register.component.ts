import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // registerForm = new FormGroup({
  //   firstName: new FormControl(),
  //   lastName: new FormControl(),
  //   gender: new FormControl(),
  // });

  constructor(private registerFb: FormBuilder) { }

  ngOnInit() {
  }

  registerForm = this.registerFb.group({
    firstName : ['', Validators.required],
    lastName : [''],
    gender : ['', Validators.required],
    dob:['', Validators.required],
    mobile:['', [Validators.required, Validators.minLength(10)]],
    email:[''],
    street: [''],
    city:[''],
    district:[''],
    postalcode:[''],
    state:['',[Validators.required]],
    country:['']
  });

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get gender() {
    return this.registerForm.get('gender');
  }

  get dob() {
    return this.registerForm.get('dob');
  }

  get mobile() {
    return this.registerForm.get('mobile');
  }
}
