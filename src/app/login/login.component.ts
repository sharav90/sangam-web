import { Component, OnInit } from "@angular/core";
import { Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginService } from "../service/login.service";
import { Authentication } from "../model/authentication";
import { MatDialog } from "@angular/material";
import { SangamDialogComponent } from "../sangam-dialog/sangam-dialog.component";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  isLoginErr: boolean = false;
  errMsg: string = "Invalid username or password";
  authRequest = new Authentication("", "");

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private loginSvc: LoginService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {}

  loginForm = this.formBuilder.group({
    username: ["", Validators.required],
    password: ["", Validators.required]
  });

  get username() {
    return this.loginForm.get("username");
  }

  get password() {
    return this.loginForm.get("password");
  }

  onLogin() {
    this.authRequest = new Authentication(
      this.username.value,
      this.password.value
    );
    this.loginSvc.jwtAuthenticate(this.authRequest).subscribe(
      data => {
        this.isLoginErr = false;
        this.loanSangamDialog();
        this.router.navigate(["dashboard"]);
      },
      error => {
        this.errMsg = "Invalid username/email id or password";
        this.isLoginErr = true;
      }
    );
  }

  loanSangamDialog() {
    console.log("Sangam Dialog start...");
    let sangamDialog = this.dialog.open(SangamDialogComponent, {
      height: "345px",
      width: "700px"
    });
    sangamDialog.afterClosed().subscribe(result => {
      console.log("The dialog was closed", result);
    });
  }
}
