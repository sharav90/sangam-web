import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatDialog, MatTable } from "@angular/material";
import { MatTableDataSource, MatSort, MatPaginator } from "@angular/material";
import { PaymentDialogComponent } from "../payment-dialog/payment-dialog.component";

export interface PaymentDto {
  name: string;
  savingAmount: number;
  loanAmount: number;
  loanRemAmount: number;
}

const ELEMENT_DATA: PaymentDto[] = [
  { savingAmount: 1023, name: "A", loanAmount: 0, loanRemAmount: 0 },
  { savingAmount: 2303, name: "B", loanAmount: 10000, loanRemAmount: 7000 },
  { savingAmount: 3404, name: "C", loanAmount: 0, loanRemAmount: 0 },
  { savingAmount: 4555, name: "D", loanAmount: 20000, loanRemAmount: 5000 },
  { savingAmount: 5444, name: "E", loanAmount: 15000, loanRemAmount: 2500 },
  { savingAmount: 5444, name: "E", loanAmount: 15000, loanRemAmount: 2500 }
];

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class PaymentComponent implements OnInit {
  displayedColumns: string[] = [
    "name",
    "savingAmount",
    "loanAmount",
    "loanRemAmount",
    "getdetails"
  ];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getRecord(name) {
    alert(name);
  }

  loadPaymentDialog(name) {
    console.log("***********");
    console.log(name);
    console.log("***********");
    let paymentDialog = this.dialog.open(PaymentDialogComponent, {
      height: "480px",
      width: "700px",
      data: name
    });
    paymentDialog.afterClosed().subscribe(result => {
      console.log("The dialog was closed", result);
    });
  }
}
