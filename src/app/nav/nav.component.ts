import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  opened = true;

  constructor(private loginSvc: LoginService, private router: Router) { }

  ngOnInit() {
  }

  navBarToggle() {
    if(this.opened) {
      this.opened = false;
    } else {
      this.opened = true;
    }
  }

  logout() {
    this.loginSvc.logout();
    this.router.navigate(['login']);
  }

  welcomeUser(): string {
    return 'Welcome: ' + this.loginSvc.getLoggedInUsername();
  }
}
