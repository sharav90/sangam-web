import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NavComponent } from "./nav/nav.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MaterialModule } from "./material/material.module";
import { ErrorComponent } from "./error/error.component";
import { EditprofileComponent } from "./editprofile/editprofile.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegisterComponent } from "./register/register.component";
import { SettingsComponent } from "./settings/settings.component";
import { ReportComponent } from "./report/report.component";
import { PaymentComponent } from "./payment/payment.component";
import { PaymentDialogComponent } from "./payment-dialog/payment-dialog.component";
import { HttpInterceptorService } from "./service/http-interceptor.service";
import { SangamDialogComponent } from "./sangam-dialog/sangam-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    DashboardComponent,
    ErrorComponent,
    EditprofileComponent,
    RegisterComponent,
    SettingsComponent,
    ReportComponent,
    PaymentComponent,
    PaymentDialogComponent,
    SangamDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  entryComponents: [PaymentDialogComponent, SangamDialogComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
