import { Component, OnInit } from "@angular/core";
import { OrganizerService } from "../service/organizer.service";
import { USER_NAME } from "../util/app.constants";
import { Organizer } from "../model/organizer";
import { MatDialog } from "@angular/material";
import { SangamDialogComponent } from "../sangam-dialog/sangam-dialog.component";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
