export const LOGGED_IN_USER = 'USERNAME';
export const JWT_TOKEN = 'TOKEN';
export const USER_NAME = 'USER_NAME';

export const ORGANIZER_ID = 'ORGANIZER_ID';
export const SANGAM_ID = 'SANGAM_ID';

// REST CONTEXT PATH
export const WEBSERVICE_CONTEXT_PATH    = 'http://localhost:5000';
export const AUTHENTICATE_API           = WEBSERVICE_CONTEXT_PATH + '/authenticate';

export const GET_ORGANIZER_BY_ID_API = WEBSERVICE_CONTEXT_PATH + '/sangam/organizer';
