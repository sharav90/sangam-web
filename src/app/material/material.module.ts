import { NgModule } from '@angular/core';
import { 
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatInputModule,
  MatButtonModule,
  MatRadioModule,
  MatSelectModule,
  MatListModule,
  MatDialogModule,
  MatIconModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatDividerModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatExpansionModule
} from '@angular/material';

const material = [
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatInputModule,
  MatButtonModule,
  MatRadioModule,
  MatSelectModule,
  MatListModule,
  MatDialogModule,
  MatIconModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatDividerModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatExpansionModule
];

@NgModule({
  imports: [ material ],
  exports: [ material ]
})
export class MaterialModule { }
